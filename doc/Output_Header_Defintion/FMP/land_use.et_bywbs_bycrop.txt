    
PRINT ET_BYWBS_BYCROP Output file from FMP LAND_USE Block

Writes for each time step crop evapotranspiration (ET) information 
by WBS and by crop (land use type). Each crop within the WBS 
is summarized separately. Bare-soil evaporation by WBS also is 
included in the output.


/-------------------------------------\
|                                     |
|    Text File Header Definition      |
|                                     |
\-------------------------------------/

PER                        is the stress period number
STP                        is the time step number
WBS                        is the Water Balance Subregion (Farm) ID number
CROP                       is the Land-use (Crop) ID number
CROP_NAME                  is the name of the specific crop
AREA              [L2]     is the area of land the crop occupies for the specified WBS
IRRIGATED_AREA    [L2]     is the area of land the crop occupies that is irrigated for the specified WBS
EFFICIENCY                 is the calculated irrigation efficiency (OFE), which is the crop irrigation requirement / irrigation water demand.
ETref             [L3/T]   is the reference evapotranspiration of the area occupied by the crop. Note ETref = ETpot if Kc=1.
ETpot             [L3/T]   is the potential crop evapotranspiration, which is the either user-specified consumptive use (CU), or crop coeficient times reference evapotranspiration (Kc*ETref), or their sum.
ETact             [L3/T]   is the actual crop evapotranspiration based on water supply
DELT              [T]      is the time step length
DYEAR                      is the date at the end of the time step as a decimal year
DATE_START                 is the starting calendar date of the time step in the form: yyyy-mm-ddThh:mm:ss


/----------------------------\
|                            |
|   Additional Information   |
|                            |
\----------------------------/


Date output is formatted using ISO 8601-1:2019 standard without a time zone designation:
https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations

The general format is:
yyyy-mm-ddThh:mm:ss
                   where:
                         yyyy is the four digit Gregorian year
                         mm   before the T is the two digit month number       (01 to 12)
                         dd   is the two digit day of the month                (01 to {28, 29, 30, 31}, depending on the month)
                         T    calendar date and 24-hour clock time separator
                         hh   is the hour within the day                       (00 to 23 hour)
                         mm   after the T is the two digit minutes in the hour (00 to 59 minute)
                         ss   is the two digit seconds of the minute           (00 to 59 second)

Output is in model units that uses an
   L as a place holder for Length, L2 is for area, L3 is for volume and
   T as a place holder for Time

which are defined in the DIS package as being:
   L: Feet, Meters, Centimeters, and Undefined
   T: Seconds, Minutes, Hours, Days, Years, and Undefined